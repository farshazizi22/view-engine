const express = require("express");
const app = express();
const port = 5000;

app.engine("html", require("ejs").renderFile);
app.set("View engine", "ejs");
// app.use(express.static("assets"));
// app.use(express.urlencoded({ extended: false }));

app.get("/", (req, res) => {
    res.render("index");
});

app.listen(port, () => console.log(`Web app up in localhost:${port}`));
